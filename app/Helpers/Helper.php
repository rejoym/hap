<?php

namespace App\Helpers;

class Helper {

  /**
   * [setNavActive description]
   * @param [type] $item_name [description]
   */
  public static function setNavActive($item_name) {
    $last_url = last(request()->segments());
    if ($last_url == '') {
      $last_url = 'home';
    }
    if ($last_url == $item_name) {
      return "active";
    }
  }

}

?>