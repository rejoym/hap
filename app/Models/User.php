<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{

  /**
   * arrays that are mass assignable
   * @var arrays
   */
  protected $fillable = ['username', 'password', 'email'];

/**
 * one to one relation with photographer model
 * @param none
 * @return instance of photographer model
 */
  public function photographers() {
    return $this->hasOne('App\Models\Photographer');
  }

/**
 * one to one relation with customer model
 * @param none
 * @return instance of customer model
 */
  public function customers() {
    return $this->hasOne('App\Models\Customer');
  }

}
