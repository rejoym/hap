<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Photographer_price extends Model
{
  /**
   * arrays that are mass assignable when inserting/updating photographer_price
   * @var arrays
   */
    protected $fillable = ['genre_id', 'photographer_id', 'shoot_id', 'price', 'equipments'];

/**
 * one to many relation between photographer and photographer_price
 * @return instance of photographer model
 */
    public function photographers() {
      return $this->belongsTo('App\Models\Photographer');
    }

/**
 * one to many relation between genre and photographer_price
 * @return instance of genre model
 */
    public function genres() {
      return $this->belongsTo('App\Models\Genre');
    }

/**
 * one to many relation between photographer_price and shoot
 * @return instance of shoot model
 */
    public function shoots() {
      return $this->belongsTo('App\Models\Shoot');
    }
}
