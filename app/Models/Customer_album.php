<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer_album extends Model
{
  /**
   * array that are mass assignable when inserting/updating customer_album
   * @var arrays
   */
    protected $fillable = ['customer_booking_id', 'photographer_id', 'title', 'info', 'date'];

/**
 * one to many relation between customer_booking and customer_album
 * @return [type] [description]
 */
    public function customerBooking() {
      return $this->belongsTo('App\Models\Customer_booking');
    }

/**
 * one to many relation between photographer and customer_album model
 * @return instance of photographer model
 */
    public function photographer() {
      return $this->belongsTo('App\Models\Photographer');
    }

/**
 * one to many relation between customer_album and customer_album_photo
 * @return instance of customer_album_photo model
 */
    public function customerAlbumPhotos() {
      return $this->hasMany('App\Models\Customer_album_photo');
    }
}
