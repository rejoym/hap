<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Genre_photographer_photo extends Model
{
  /**
   * arrays that are mass assignable when inserting/updating genre_photographer_photo
   * @var array
   */
    protected $fillable = ['genre_photographer_id', 'photo_url', 'photo_name'];

/**
 * one to many relation between genre_photographer_photo and genre_photographer
 * @return instance of genre_photographer model
 */
    public function genrePhotographers() {
      return $this->belongsTo('App\Models\Genre_photographer');
    }
}
