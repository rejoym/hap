<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer_booking extends Model
{
  /**
   * arrays that are mass assignable when inserting/updating customer_booking
   * @var [type]
   */
    protected $fillable = ['customer_id', 'location', 'date', 'start_time', 'shoot_id', 'match_type', 'booking_status'];

/**
 * one to many relation between customer_booking and customer
 * @return instance of customer
 */
    public function customers() {
      return $this->belongsTo('App\Models\Customer');
    }

/**
 * one to many relation between customer_booking and customer_booking_photographer
 * @return instance of customer_booking_photographer
 */
    public function customerBookingPhotographers() {
      return $this->hasMany('App\Models\Customer_booking_photographer');
    }

/**
 * one to many relation between customer_booking and booking_payment
 * @return instance of booking_payment model
 */
    public function bookingPayments() {
      return $this->hasMany('App\Models\Booking_payment');
    }

/**
 * ome to many relation between customer_booking and customer_album
 * @return instamce of customer_album
 */
    public function customerAlbums() {
      return $this->hasMany('App\Models\Customer_album');
    }

}
