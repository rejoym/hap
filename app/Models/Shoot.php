<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Shoot extends Model
{
  /**
   * arrays that are mass assignable when inserting/updating shoot
   * @var array
   */
    protected $fillable = ['name'];

/**
 * one to many relation between photographer_price and shoot
 * @return instance of photographer_price
 */
    public function photographerPrices() {
      return $this->hasMany('App\Models\Photographer_price');
    }

}
