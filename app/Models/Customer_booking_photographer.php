<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer_booking_photographer extends Model
{
  /**
   * arrays that are mass assignable when inserting/updating customer_booking_photographer
   * @var array
   */
    protected $fillable = ['customer_booking_id', 'photographer_id'];

/**
 * one to many relation between customer_booking_photographer and customer_booking
 * @return instance of customer_booking
 */
    public function customerBookings() {
      return $this->belongsTo('App\Models\Customer_booking');
    }

/**
 * one to many relation between customer_booking_photographer and photographer
 * @return instance of photographer model
 */
    public function photographer() {
      return $this->belongsTo('App\Models\Photographer');
    }
}
