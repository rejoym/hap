<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostRegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first-name' => 'required',
            'last-name' => 'required',
            'country' => 'required',
            'city' => 'required',
            'address' => 'required',
            'gender' => 'required',
            'contact-number' => 'required',
            'email' => 'required|email|unique:users,email',
            'birth-date' => 'required',
            'user_type' => 'required',
            'username' => 'required|unique:users,username|min:6',
            'password' => 'required|min:6'
            'confirm-password' => 'required|same:password'
        ];
    }
}
