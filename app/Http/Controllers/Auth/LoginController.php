<?php

namespace App\Http\Controllers\Auth;

use DB;
use \Auth;
use \Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\PostLoginRequest;
use Illuminate\Foundation\Auth\AuthenticatesUsers;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Where to redirect users after failed login
     *
     * @var string
     */
    protected $loginPath = '/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('guest')->except('logout');
         $this->middleware('guest', ['except' => ['logout', 'getLogout']]);
    }

    /**
     * [getLogin description]
     * @return [type] [description]
     */
    public function getLogin(\Request $request) {
        return view('frontend.login');
    }

    /**
     * function to handle post login request
     * @param  PostLoginRequest $request [form request]
     * @return [type]                    [description]
     */
    public function postLogin(PostLoginRequest $request) {
        //value of username or email field
        $useroremail = $request->input('input-username');
        //value of password field
        $psw = $request->input('input-password');
        //boolean value of remember me check box
        $remember = $request->input('remember-me') ? true : false;
        //attempt tp login the user using email or username and password and keep them logged in if keep me logged in option is selected
        if( (Auth::attempt(['username' => $useroremail, 'password' => $psw], $remember)) ||
            (Auth::attempt(['email' => $useroremail, 'password' => $psw], $remember)) ) {
            // echo "string2";die;
            $this->loggedIn();
        }

        $request->session()->flash('loginError', 'Invalid credentials!!');
        return redirect()->route('get.login')->withInput($request->except('password'));

    }

    /**
     * [loggedIn description]
     * @return [type] [description]
     */
    public function loggedIn() {
        // echo "string1";die;
        $user_info = $this->loggedInUserInfo();
        Session::put('user_info', $user_info);
        return redirect()->route('home');
    }

    /**
     * [loggedInUserInfo description]
     * @return [type] [description]
     */
    public function loggedInUserInfo() {
        $id = Auth::id();
        $user_info_detail = DB::table('photographers')->where('user_id', $id)
                                                      ->get();
        if(empty($user_info_detail)) {
            Session::flash('loginError', 'User Not Found!!');
            return redirect()->route('get.login');
        }
        return $user_info_detail;
    }

    /**
     * [getLogout description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function getLogout(Request $request) {
        $user = Auth::user();
        if($user):
            Auth::logout();
            $request->session()->flash('loggedOut', 'You have sucessfully logged out.');
            $request->session()->forget('user_info');
        endif;

        return redirect()->route('get.login');
    }

}
