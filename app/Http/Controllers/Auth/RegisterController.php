<?php

namespace App\Http\Controllers\Auth;

use App\Models\User as User;
use App\Models\Photographer as Photographer;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
    /**
     * [getRegister description]
     * @return [type] [description]
     */
    public function getRegister() {
        return view('frontend.photographer.register_photographer');
    }

    /**
     * [postPhotographerRegister description]
     * @param  PostRegisterRequest $request [description]
     * @return [type]                       [description]
     */
    public function postPhotographerRegister(PostRegisterRequest $request) {
        $first_name = $request->('first-name');
        $middle_name = $request->('middle-name');
        $last_name = $request->('last-name');
        $country = $request->('country');
        $city = $request->('city');
        $address = $request->('address');
        $gender = $request->('gender');
        $contact = $request->('contact-number');
        $email = $request->('email');
        $dob = $request->('birth-date');
        $user_type = 'photographer';
        $payment_info = $request->('payment-info');
        $bank_info = $request->('bank-info');
        $acc_no = $request->('acc-no');
        $email = $request->('email');
        $username = $request->('username');
        $psw = $request->('password');
        if($request->file('identity-pic') != '') {
            $image1_rand_name = str_random(10);
            $image1_name = $image1_rand_name.time().'.'.$request->file('identity-pic')->getClientOriginalExtension();
            $request->file('identity-pic')->move(
                base_path() . '/public/assets/images' .$image1_name
            );
        }
        if($request->file('award-pic') != '') {
            $image2_rand_name = str_random(10);
            $image2_name = $image2_rand_name.time().'.'.$request->file('award-pic')->getClientOriginalExtension();
            $request->file('award-pic')->move(
                base_path() .'/public/assets/images' . $image2_name
            );
        }
        if($request->file('profile-pic') != '') {
            $image3_rand_name = str_random(10);
            $image3_name = $image3_rand_name.time().'.'.$request->file('profile-pic')->getClientOriginalExtension();
            $request->file('profile-pic')->move(
                base_path() .'/public/assets/images' .$image3_name
            );
        }

        $data_user = [
            'username' => $username,
            'password' => $psw,
            'email' => $email
        ];
        $insert_user = $this->create($data_user);

        $inserted_user_id = $insert_user->id;

        $data_user_id = [
            'user_id' => $inserted_user_id
        ];

        $this->createPhotographer($data_user_id);

        $request->session()->flash('Registered', 'New Photographer has been successfully registered');

        return redirect()->route('get.register');
    }
    // /**
    //  * Get a validator for an incoming registration request.
    //  *
    //  * @param  array  $data
    //  * @return \Illuminate\Contracts\Validation\Validator
    //  */
    // protected function validator(array $data)
    // {
    //     return Validator::make($data, [
    //         'name' => 'required|string|max:255',
    //         'email' => 'required|string|email|max:255|unique:users',
    //         'password' => 'required|string|min:6|confirmed',
    //     ]);
    // }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create($data);
    }
    /**
     * create a new photographer instance after registration
     * @param  array  $data [description]
     * @return \App\Photographer
     */
    protected function createPhotographer(array $data)
    {
        return Photographer::create($data);
    }
}
