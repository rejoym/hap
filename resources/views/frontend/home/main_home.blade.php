@extends('frontend.layouts.master')
@section('main-content')
<div class="container-fluid">
  <div class="main-body">
    <div class="row">
      <div class="col-md-8 slider-wrapper">
        <!-- Slider starts here -->
        <div id="banner-slider" class="carousel slide" data-ride="carousel">
          <ol class="carousel-indicators">
            <li data-target="#banner-slider" data-slide-to="0" class="active"></li>
            <li data-target="#banner-slider" data-slide-to="1"></li>
            <li data-target="#banner-slider" data-slide-to="2"></li>
          </ol>
          <div class="carousel-inner">
            <div class="carousel-item active">
              <img class="d-block img-responsive w-100" src="{{ asset('assets/images/1.jpg') }}" alt="First slide">
            </div>
            <div class="carousel-item">
              <img class="d-block img-responsive w-100" src="{{ asset('assets/images/2.jpg') }}" alt="Second slide">
            </div>
            <div class="carousel-item">
              <img class="d-block img-responsive w-100" src="{{ asset('assets/images/3.jpg') }}" alt="Third slide">
            </div>
          </div>
          <a class="carousel-control-prev" href="#banner-slider" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#banner-slider" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
        <!-- Slider ends here -->
        <div class="slider-overlay">
          <div class="overlay-content-wrapper">
            <p class="slider-title">Great photography shouldn't be so hard.</p>
            <p class="slider-info">Hire Any Photographer connects you with pre-vetted pro photographers for all of your needs.</p>
          </div>
        </div>
      </div>
      <div class="col-md-4 tile-wrapper register-tile">
        <h2 class="tile-title">Find Photographers</h2>
        <form action="" method="post" class="search-photographers">
          <div class="form-group">
            <small class="form-text text-muted">Select Location</small>
            <select class="form-control" name="">
              <option value="">Kathmandu</option>
              <option value="">Pokhara</option>
              <option value="">Dharan</option>
            </select>
          </div>
          <div class="form-group">
            <small class="form-text text-muted">Select Genre</small>
            <select class="form-control" name="">
              <option value="">Event</option>
              <option value="">Graduation</option>
              <option value="">Portrait</option>
              <option value="">Fashion</option>
              <option value="">Product</option>
              <option value="">Food</option>
              <option value="">Family</option>
              <option value="">Real Estate</option>
            </select>
          </div>
          <div class="form-group">
            <small id="" class="form-text text-muted">Enter Price (US$)</small>
            <input type="text" class="form-control" id="" aria-describedby="" placeholder="Price">
          </div>
          <div class="form-group">
            <small class="form-text text-muted">Select Rating</small>
            <select class="form-control" name="">
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
              <option value="4">4</option>
              <option value="5">5</option>
              <option value="6">6</option>
              <option value="7">7</option>
              <option value="8">8</option>
              <option value="9">9</option>
              <option value="10">10</option>
            </select>
          </div>
          <button type="submit" class="btn btn-primary register-btn" href="#" role="button"><i class="fa fa-search"></i>&nbsp; SEARCH NOW</button>
        </form>
      </div>
    </div>
    <div class="row our-events">
      <div class="w-100">
        <h2 class="page-title our-event-title">Instantly book a quality photographer for any shoot types</h2>
        <p class="page-description our-event-description">We have photographers for all specialties (or at least all the G-rated ones). Click to see portfolio samples.</p>
      </div>
      <div class="cards-wrapper row">
        <div class="page-card col-md-3">
          <img class="card-img-top" src="{{ asset('assets/images/event.jpg') }}" alt="Card image cap">
          <div class="page-card-body">
            <h5 class="page-card-title">Event</h5>
          </div>
        </div>
        <div class="page-card col-md-3">
          <img class="card-img-top" src="{{ asset('assets/images/graduation.jpg') }}" alt="Card image cap">
          <div class="page-card-body">
            <h5 class="page-card-title">Graduation</h5>
          </div>
        </div>
        <div class="page-card col-md-3">
          <img class="card-img-top" src="{{ asset('assets/images/portrait.jpg') }}" alt="Card image cap">
          <div class="page-card-body">
            <h5 class="page-card-title">Portrait</h5>
          </div>
        </div>
        <div class="page-card col-md-3">
          <img class="card-img-top" src="{{ asset('assets/images/fashion.jpg') }}" alt="Card image cap">
          <div class="page-card-body">
            <h5 class="page-card-title">Fashion</h5>
          </div>
        </div>
        <div class="page-card col-md-3">
          <img class="card-img-top" src="{{ asset('assets/images/product.jpg') }}" alt="Card image cap">
          <div class="page-card-body">
            <h5 class="page-card-title">Product</h5>
          </div>
        </div>
        <div class="page-card col-md-3">
          <img class="card-img-top" src="{{ asset('assets/images/food.jpg') }}" alt="Card image cap">
          <div class="page-card-body">
            <h5 class="page-card-title">Food</h5>
          </div>
        </div>
        <div class="page-card col-md-3">
          <img class="card-img-top" src="{{ asset('assets/images/family.jpg') }}" alt="Card image cap">
          <div class="page-card-body">
            <h5 class="page-card-title">Family</h5>
          </div>
        </div>
        <div class="page-card col-md-3">
          <img class="card-img-top" src="{{ asset('assets/images/realestate.jpg') }}" alt="Card image cap">
          <div class="page-card-body">
            <h5 class="page-card-title">Real Estate</h5>
          </div>
        </div>
      </div><!-- .cards-wrapper, .row ends here -->
    </div>
    <div class="row overview-section">
      <div class="col-md-4 overview-tile">
        <div class="content-tile-wrapper">
          <div class="tile-content tile-vertical-content">
            <p><i class="fa fa-hand-o-right"></i><p>
            <h2 class="tile-title white-color overview-sub-title">Find Photographers</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam, itaque!</p>
          </div><!-- .tile-content, .tile-vertical-content end here -->
        </div>
      </div>
      <div class="col-md-4 overview-tile">
        <div class="content-tile-wrapper">
          <div class="tile-content tile-vertical-content">
            <p><i class="fa fa-paper-plane-o"></i><p>
            <h2 class="tile-title white-color overview-sub-title">Make Bookings</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam, itaque!</p>
          </div><!-- .tile-content, .tile-vertical-content end here -->
        </div>
      </div>
      <div class="col-md-4 overview-tile">
        <div class="content-tile-wrapper">
          <div class="tile-content tile-vertical-content">
            <p><i class="fa fa-handshake-o"></i><p>
            <h2 class="tile-title white-color overview-sub-title">Assure Quality</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam, itaque!</p>
          </div><!-- .tile-content, .tile-vertical-content end here -->
        </div>
      </div>
    </div>
    <div class="row photographers-section">
      <div class="col-md-4 common-tiles popular-photographers-wrapper">
        <a href="#" class="tile-link">
          <div class="hover-bg hover-color-blue"></div>
          <div class="row event-tile main-tile bottom-shadow">
            <div class="carousel slide" data-ride="carousel" id="photographers-carousel" data-interval="5000">
              <div class="carousel-inner" role="listbox">
                <div class="carousel-item">
                  <img class="d-block img-fluid" src="{{ asset('assets/images/p1.jpg') }}" alt="First slide">
                </div><!-- .carousel-item -->
                <div class="carousel-item active">
                  <img class="d-block img-fluid" src="{{ asset('assets/images/p2.jpg') }}" alt="Second slide">
                </div><!-- .carousel-item -->
                <div class="carousel-item">
                  <img class="d-block img-fluid" src="{{ asset('assets/images/p3.jpg') }}" alt="Third slide">
                </div><!-- .carousel-item -->
              </div><!-- .carousel-inner ends -->
            </div><!-- .carousel, .slide ends -->
            <!-- event contents -->
            <div class="overlap-section">
              <div class="sub-overlap">
                <div class="tile-content content-sub-overlap">
                  <h2 class="tile-title white-color">Photographers</h2>
                  <p>View all photographers which are currently available in hire any photographer.</p>
                </div>
              </div><!-- .sub-overlap ends -->
            </div><!-- .overlap-section ends -->
          </div><!-- .row, .event-tile ends -->
        </a>
      </div>
      <div class="col-md-4 common-tiles book-photographer-wrapper">
        <div class="row book-tile padding-tile">
          <div class="content-tile-wrapper">
            <div class="tile-content tile-vertical-content">
              <h2 class="tile-title white-color">Not Registered Yet?</h2>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam, itaque!</p>
              <a class="btn book-photographer-nav book-photographer-tile" href="#" role="button"><i class="fa fa-male"></i>&nbsp; Register Photographer</a>
            </div><!-- .tile-content, .tile-vertical-content end here -->
          </div><!-- .content-tile-wrapper ends -->
        </div>
      </div>
      <div class="col-md-4 common-tiles customer-reviews-wrapper">
        <a href="#" class="tile-link">
          <div class="hover-bg hover-color-blue"></div>
          <div class="row event-tile main-tile bottom-shadow">
            <div class="carousel slide" data-ride="carousel" id="photographers-carousel" data-interval="3000">
              <div class="carousel-inner" role="listbox">
                <div class="carousel-item">
                  <img class="d-block img-fluid" src="{{ asset('assets/images/c1.jpg') }}" alt="First slide">
                </div><!-- .carousel-item -->
                <div class="carousel-item active">
                  <img class="d-block img-fluid" src="{{ asset('assets/images/c2.jpg') }}" alt="Second slide">
                </div><!-- .carousel-item -->
                <div class="carousel-item">
                  <img class="d-block img-fluid" src="{{ asset('assets/images/c3.jpg') }}" alt="Third slide">
                </div><!-- .carousel-item -->
                <div class="carousel-item">
                  <img class="d-block img-fluid" src="{{ asset('assets/images/c4.jpg') }}" alt="Third slide">
                </div><!-- .carousel-item -->
              </div><!-- .carousel-inner ends -->
            </div><!-- .carousel, .slide ends -->
            <!-- event contents -->
            <div class="overlap-section">
              <div class="sub-overlap">
                <div class="tile-content content-sub-overlap">
                  <h2 class="tile-title white-color">Happy Customers</h2>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor iqua.</p>
                </div>
              </div><!-- .sub-overlap ends -->
            </div><!-- .overlap-section ends -->
          </div><!-- .row, .event-tile ends -->
        </a>
      </div>
    </div>
    <div class="row newsletter-section">
      <div class="col-md-8 common-tiles recent-shoots-wrapper">
        <a href="#" class="tile-link">
          <div class="hover-bg hover-color-red"></div>
          <div class="row event-tile main-tile bottom-shadow">
            <div class="carousel slide" data-ride="carousel" id="recent-shoots-carousel" data-interval="4000">
              <div class="carousel-inner" role="listbox">
                <div class="carousel-item">
                  <img class="d-block img-fluid" src="{{ asset('assets/images/rs1.jpg') }}" alt="First slide">
                </div><!-- .carousel-item -->
                <div class="carousel-item active">
                  <img class="d-block img-fluid" src="{{ asset('assets/images/rs2.jpg') }}" alt="Second slide">
                </div><!-- .carousel-item -->
                <div class="carousel-item">
                  <img class="d-block img-fluid" src="{{ asset('assets/images/rs3.jpg') }}" alt="Third slide">
                </div><!-- .carousel-item -->
                <div class="carousel-item">
                  <img class="d-block img-fluid" src="{{ asset('assets/images/rs4.jpg') }}" alt="Third slide">
                </div><!-- .carousel-item -->
              </div><!-- .carousel-inner ends -->
            </div><!-- .carousel, .slide ends -->
            <!-- event contents -->
            <div class="overlap-section">
              <div class="sub-overlap">
                <div class="tile-content content-sub-overlap">
                  <h2 class="tile-title white-color">Recent Photo Shoots</h2>
                  <p>View all the recent photo shoots done by our photographers.</p>
                </div>
              </div><!-- .sub-overlap ends -->
            </div><!-- .overlap-section ends -->
          </div><!-- .row, .event-tile ends -->
        </a>
      </div>
      <div class="col-md-4 newsletter-wrapper common-tiles">
        <div class="row newsletter-tile padding-tile bg-cover">
          <div class="content-tile-wrapper signup-newsletter">
            <div class="tile-content tile-vertical-content">
              <h2 class="tile-title white-color">Newsletter Sign up</h2>
              <p>Get the latest news and updates</p>
              <a class="btn btn-primary book-photographer-nav book-photographer-tile sub-btn" href="#" role="button">Subscribe</a>
            </div><!-- .tile-content, .tile-vertical-content end here -->
          </div><!-- .content-tile-wrapper ends -->
        </div>
      </div>
    </div>
@stop