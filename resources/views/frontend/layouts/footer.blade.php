        <div class="row footer-section">
          <footer>
            <div class="row content-footer d-none d-md-block">
              <!-- footer menu -->
              <ul class="footer-menu">
                <li><a href="#">About Us</a></li>
                <li><a href="#">Blog</a></li>
                <li><a href="#">Privacy Policy</a></li>
                <li><a href="#">Terms</a></li>
                <li><a href="#">Cookies</a></li>
                <li><a href="#">Contact</a></li>
                <li><a href="#">Faq</a></li>
              </ul><!-- footer menu ends -->
              <p class="copyright pull-right">&copy; 2018 Hire Any Photographer <span>All Rights Reserved.</span></p>
            </div><!-- .row, .content-footer ends -->
          </footer>
        </div>
      </div><!-- .main-body ends here -->
    </div><!-- .container-fluid ends here -->
    <script src="{{ asset('assets/js/jquery-3.3.1.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/js/app.js') }}"></script>
  </body>
</html>
