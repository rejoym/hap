<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/font-awesome.min.css') }}">
    <link href="https://fonts.googleapis.com/css?family=Cabin|Source+Sans+Pro" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    <title>Hire Any Photographer</title>
  </head>
  <body>
    <div class="fixed-sidebar">
      <div class="sidebar-content-wrapper">
        <div class="top-content">
          <div class="site-header">
            <div id="logo">
              <img src="{{ asset('assets/images/logo.png') }}" alt="">
              <h1>Hire Any Photographer</h1>
            </div><!-- #logo ends here -->
            <p class="site-description">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
          </div><!-- .site-header ends here -->
          <ul class="site-menu">
            <li class="site-menu-items {!! Helper::setNavActive('home') !!}"><a href="home">Home</a></li>
            <li class="site-menu-items"><a href="#">Shoot Types</a></li>
            <li class="site-menu-items"><a href="#">How it works</a></li>
            <li class="site-menu-items"><a href="#">Photographers</a></li>
            <li class="site-menu-items"><a href="#">About Us</a></li>
            @if(!Auth::check())
              <li class="site-menu-items {!! Helper::setNavActive('login') !!}"><a href="login">Member Login</a></li>
            @else
              <li class="site-menu-items {!! Helper::setNavActive('logout') !!}"><a href="{{ route('get.logout') }}">Logout</a></li>
            @endif
          </ul><!-- .site-menu ends here -->
          <a class="btn btn-primary btn-block book-photographer-nav" href="#" role="button"><i class="fa fa-paper-plane"></i>&nbsp; Book a Photographer</a>
        </div><!-- .top-content ends here -->
        <div class="bottom-content">
          <p class="copyright-text">Find us elsewhere,</p>
          <div class="social-media-content">
            <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
            <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
            <a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
          </div><!-- .social-media-content ends here -->
          <!-- <p class="copyright-text">&copy; Hire Any Photographer | All rights reserved.</p> -->
        </div><!-- .bottom-content ends here -->
      </div><!-- .sidebar-content-wrapper ends here -->
    </div><!-- .fixed-sidebar ends here -->