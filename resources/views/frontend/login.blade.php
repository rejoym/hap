@extends('frontend.layouts.master')
@section('main-content')
<div class="container-fluid">
  <div class="main-body">
    <div class="row">
      <div class="col-md-12 login-wrapper">
        <div class="login-heading">
          <h2 class="login-title">You are logged Out</h2>
          <p>In order to proceed you need to login.</p>
        </div>
        <div class="row user-login-box padding-tile">
          <div class="user-login-content">
            <div class="row">
              <div class="col-md-8 login-main-content">
                <h2 class="login-title">USER LOG-IN</h2>
                <span class="error">
                  {{Session::get('loginError')}}
                </span>
                <span class="success">
                  {{Session::get('loggedOut')}}
                </span>
                <form class="member-login" action="" id="" method="post">
                  {!! csrf_field() !!}
                  <div class="form-group{{ $errors->has('input-username') ? 'has-error' : '' }}">
                    <label for="input-email">Login Username/Email</label>
                    <input type="text" name="input-username" id="input-email" class="form-control" aria-describedby="Login ID" value="{{ old('input-username')}}">
                    <span>{{ $errors->first('input-username') }}</span>
                  </div><!-- .form-group ends -->
                  <div class="form-group{{ $errors->has('input-password') ? 'has-error' : '' }}">
                    <label for="input-psw">Password: </label>
                    <input type="password" name="input-password" id="input-psw" class="form-control" aria-describedby="Login Password">
                    <span>{{ $errors->first('input-password') }}</span>
                  </div><!-- .form-group ends -->
                  <div class="">
                   <label class="keep-me"><input type="checkbox" name="remember-me"> Keep me logged in</label>
                  </div>
                  <button type="submit" class="btn btn-user-login"><i class="fa fa-sign-in"></i> Login</button>
                  <button type="button" class="btn btn-user-signup"><a href="{{route('get.register')}}" class="btn-signup"><i class="fa fa-user-plus"></i> Sign Up</a></button>
                </form><!-- #plaer-login-form ends here -->
                <p class="forgot-pw"><a href="#">Forgot your password?</a></p>
              </div>
              <div class="col-md-4 login-right-section">
                <div class="">
                  <p class="login-info">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda, alias. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi facere laborum at aspernatur, placeat commodi?</p>
                </div>
              </div>
            </div>
          </div><!-- .player-login-content ends -->
        </div>
      </div>
    </div>

@stop