<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerAlbumsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_albums', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_booking_id')->unsigned();
            $table->integer('photographer_id')->unsigned();
            $table->string('title', 255);
            $table->string('info', 255);
            $table->date('date');
            $table->timestamps();
        });
        Schema::table('customer_albums', function (Blueprint $table) {
            $table->foreign('customer_booking_id')
                ->references('id')
                ->on('customer_bookings')
                ->onUpdate('cascade');
            $table->foreign('photographer_id')
                ->references('id')
                ->on('photographers')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_albums');
    }
}
