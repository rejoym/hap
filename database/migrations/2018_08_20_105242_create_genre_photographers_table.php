<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGenrePhotographersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('genre_photographers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('genre_id')->unsigned();
            $table->integer('photographer_id')->unsigned();
            $table->timestamps();
        });
        Schema::table('genre_photographers', function (Blueprint $table) {
            $table->foreign('genre_id')
                ->references('id')
                ->on('genres')
                ->onUpdate('cascade');
            $table->foreign('photographer_id')
                ->references('id')
                ->on('photographers')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('genre_photographers');
    }
}
