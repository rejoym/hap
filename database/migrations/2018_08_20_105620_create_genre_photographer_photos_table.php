<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGenrePhotographerPhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('genre_photographer_photos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('genre_photographer_id')->unsigned();
            $table->string('photo_url', 255);
            $table->string('photo_name', 255);
            $table->timestamps();
        });
        Schema::table('genre_photographer_photos', function (Blueprint $table) {
            $table->foreign('genre_photographer_id')
                ->references('id')
                ->on('genre_photographers')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('genre_photographer_photos');
    }
}
