<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('frontend.home.main_home');
});

Route::get('login', ['uses' => 'Auth\LoginController@getLogin', 'as' => 'get.login']);
Route::post('login', ['uses' => 'Auth\LoginController@postLogin', 'as' => 'post.login']);
Route::get('logout', ['uses' => 'Auth\LoginController@getLogout', 'as' => 'get.logout']);
Route::get('register', ['uses' => 'Auth\RegisterController@getRegister', 'as' => 'get.register']);
Route::get('home', ['uses' => 'Home\HomeController@getIndex', 'as' => 'home']);